﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using LeadManagementSystem.Models;

namespace LeadManagementSystem.Repo
{
    public class LoginRepo
    {
        public static DataTable LoginCheck(User oUser)
        {
            string ConnectionString = ConfigurationManager.ConnectionStrings["MySqlConnectionString"].ConnectionString;
            SqlConnection sqlconnection = new SqlConnection(ConnectionString);
            string SpName = "CheckLogin";
            SqlCommand sqlcommand = new SqlCommand(SpName, sqlconnection);
            sqlcommand.CommandType = System.Data.CommandType.StoredProcedure;
            DataTable dt = new DataTable();
            
            try
            {
                sqlcommand.Parameters.AddWithValue("@_Email", oUser.Email);
                sqlcommand.Parameters.AddWithValue("@_Password", oUser.Passwd);

                SqlDataAdapter sda = new SqlDataAdapter(sqlcommand);
                sqlconnection.Open();

                sda.Fill(dt);
                sqlconnection.Close();
                return dt;
            }
            catch (Exception ex)
            {
                sqlconnection.Close();
                throw;
            }
            finally
            {
                if (sqlconnection.State == ConnectionState.Open) sqlconnection.Close();
            }
        }
    }
}