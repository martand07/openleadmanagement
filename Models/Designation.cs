﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LeadManagementSystem.Models
{
    [Table("DesignationMaster")]
    public class Designation
    {
        public int DesignationId { set; get; }

        [MaxLength(512)]
        public string DesignationTitle { set; get; }

    }
}