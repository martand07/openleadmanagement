﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LeadManagementSystem.Models
{
    [Table("tblUser")]
    public class User
    {
        [Key]
        public int UserId { set; get; }

        [MaxLength(512)]
        public string UserName { set; get; }


        [MaxLength(50)]
        public string FirstName { set; get; }

        [MaxLength(50)]
        public string LastName { set; get; }

        [MaxLength(512)]
        [Required]
        public string Email { set; get; }


        [MaxLength(512)]
        [DataType(DataType.Password)]
        public string Passwd { set; get; }

        public DateTime CreatedOn { set; get; }
        public DateTime UpdatedOn { set; get; }
        public DateTime DeletedOn { set; get; }

        public int CreatedBy { set; get; }
        public int UpdatedBy { set; get; }
        public int DeletedBy { set; get; }
        public int DepartmentId { set; get; }
        public decimal Salary { set; get; }

        public int DesignationId { set; get; }


        public bool isSuccess { set; get; }
    }
}