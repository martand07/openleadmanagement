﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LeadManagementSystem.Models
{
    public class Lead
    {
        public int LeadId { set; get; }

        [MaxLength(512)]
        public string Name { set; get; }

        [MaxLength(512)]
        public string LeadSource { set; get; }
        public string LeadUrl { set; get; }
        public string LeadDetails { set; get; }
        
        [NotMapped]
        public string GeneratedBy { set; get; }
    }
}