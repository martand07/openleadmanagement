﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LeadManagementSystem.Models
{

    [Table("LeadAssign")]
    public class LeadAssign
    {
       public int LeadAssignId { set; get; }
        public int LeadId { set; get; }
        public int AssignTo { set; get; }
        public int AssignBy { set; get; }
        public DateTime AssignedOn { set; get; }
        public int ReadFlag { set; get; }
        public int RecordStatus { set; get; }
    }
}