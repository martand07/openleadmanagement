﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LeadManagementSystem.Models
{
    [Table("DepartmentMaster")]
    public class Department
    {
        [Key]
        public int DepartmentId { set; get; }

        [MaxLength(250)]
        public string DepartmentName { set; get; }

        public DateTime CreatedOn { set; get; }
        public DateTime UpdatedOn { set; get; }
        public DateTime DeletedOn { set; get; }

        public int CreatedBy { set; get; }
        public int UpdatedBy { set; get; }
        public int DeletedBy { set; get; }
   
    }
}