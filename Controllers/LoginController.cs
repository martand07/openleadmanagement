﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using LeadManagementSystem.Models;
using LeadManagementSystem.Repo;
using System.Data;

namespace LeadManagementSystem.Controllers
{
    public class LoginController : Controller
    {
        // GET: Login
        
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(User oUser)
        {
            //Validate User credential in database. 
            DataTable obj = new DataTable();


            obj = LoginRepo.LoginCheck(oUser);

            if(obj != null && obj.Rows.Count > 0)
            {
                for (int i = 0; i < obj.Rows.Count; i++)
                {
                    oUser.isSuccess = true;
                    oUser.UserId = Convert.ToInt32(obj.Rows[i]["UserId"]);
                    oUser.FirstName = Convert.ToString(obj.Rows[i]["FirstName"]);
                    oUser.LastName = Convert.ToString(obj.Rows[i]["LastName"]);
                    //oUser.DesignationId = Convert.ToInt32(obj.Rows[i]["DesignationId"]);
                }
            }
            else
            {
                oUser.isSuccess = false;
            }
            
            return Json(oUser, JsonRequestBehavior.AllowGet);
        }
    }
}